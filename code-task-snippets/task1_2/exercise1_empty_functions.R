# Task: add the missing function parts and the xpaths(shorttext, transcript, date) for the pages. Refer to the provided comments in the functions.
# Level: medium

# Include necessary packages
# Note: Must be installed!
Packages <- c("readtext", "rstudioapi", "tidyr", "readr", "rvest", "stringr", "jsonlite")
lapply(Packages, library, character.only = TRUE)

# Set current working directory correctly to include other functions (only works in RStudio!!)
setwd(dirname(rstudioapi::getSourceEditorContext()$path))
source("./util.R")

# Define function to scrape archive pages
scrape_regpk_archiv <- function(url) {
  
  # download item into data subfolder with unique identifier as file name
  
  # extract shorttext
  #regpk_shorttext <- 
  
  # extract transcript
  #regpk_fulltext <-  
  
  # extract date
  #regpk_date <- 
    
  # wait some random seconds between 5 and 10 to spare the server
  
  return (list(href=url,shorttext=regpk_shorttext,fulltext=regpk_fulltext,date=regpk_date))
}

# Define function to scrape regpk pages
scrape_regpk_current <- function(url) {
  
  # download item into data subfolder with unique identifier as file name

  # extract shorttext
  #regpk_shorttext <- ...
  
  # extract transcript
  #regpk_fulltext <- 
  
  # extract date
  #regpk_date <-
  
  # wait some random seconds between 5 and 10 to spare the server
  
  return (list(href=url,shorttext=regpk_shorttext,fulltext=regpk_fulltext,date=regpk_date))
  
}

url_regpk_current <- "https://www.bundesregierung.de/breg-de/suche/regierungspressekonferenz-vom-4-dezember-843536"
url_regpk_archiv <- "https://archiv.bundesregierung.de/archiv-de/dokumente/regierungspressekonferenz-vom-29-november-842826"


# Uncomment for testing:
#scrape_regpk_archiv(url_regpk_archiv) %>% jsonlite::toJSON( encoding="UTF-8", auto_unbox=TRUE) %>% readr::write_file("./result.json")
#scrape_regpk_current(url_regpk_current) %>% jsonlite::toJSON( encoding="UTF-8", auto_unbox=TRUE) %>% readr::write_file("./result.json")
