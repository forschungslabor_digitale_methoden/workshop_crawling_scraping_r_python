# -*- coding: utf-8 -*-
import scrapy
import datetime, locale
from scrapy.utils.markup import remove_tags

class ArchivBundesregierungSpider(scrapy.Spider):
    # 
    name = 'archiv_bundesregierung'
    # 
    allowed_domains = ['archiv.bundesregierung.de']
    # 
    main_url = "https://archiv.bundesregierung.de"
    # 
    start_url = "https://archiv.bundesregierung.de/archiv-de/dokumente/69986!search?page=720"
    # 
    links_regpk_selector = "regierungspressekonferenz"

    def start_requests(self):
        """
        
        :return: 
        """
        yield scrapy.Request(self.start_url, callback=self.parse_search_items)

    def parse_search_items(self, response):
        """
        
        :param response: 
        :return: 
        """
        #
        next_page_container = response.xpath('//li[@class="forward"]/a/@href')

        if next_page_container:
            #
            url_postfix = next_page_container.extract_first()
            yield scrapy.Request("{}{}".format(self.main_url, url_postfix), callback=self.parse_search_items)

        #
        search_items_container = response.xpath('//li/h3/a/@href').extract()
        for link in search_items_container:
            if self.links_regpk_selector in link:
                yield scrapy.Request("{}{}".format(self.main_url, link))

    def parse(self, response):
        """
        
        :param response:
        :return: 
        """

        # 
        regpk_shorttext = response.xpath('//div[@class="abstract"]').extract_first()
        regpk_shorttext = remove_tags(regpk_shorttext) # 
        regpk_shorttext = regpk_shorttext.replace(u'\xa0', ' ').strip() #

        # 
        regpk_fulltext = response.xpath('//div[@class="basepage_pages"]/*[not(self::div)]//text()').extract()
        # 
        regpk_fulltext = [item.replace(u'\xa0', ' ') for item in regpk_fulltext if not "" == item.strip()]

        # 
        regpk_time_extracted = response.xpath('//p[@class="date"]/text()').extract_first()
        # 
        locale.setlocale(locale.LC_ALL, "german")
        regpk_time = datetime.datetime.strptime(regpk_time_extracted.split(",")[1].strip(), '%d. %B %Y')

        # 
        yield {"href": response.url, "shorttext": regpk_shorttext, "fulltext": regpk_fulltext, "date": regpk_time.strftime("%Y-%m-%d")}