# -*- coding: utf-8 -*-
import scrapy
from scrapy.utils.markup import remove_tags
import json
from bs4 import BeautifulSoup
import datetime

class BundesregierungSpider(scrapy.Spider):
    # 
    name = 'bundesregierung'

    allowed_domains = ['www.bundesregierung.de']

    # 
    start_url_csrf = "https://www.bundesregierung.de/service/csrf"

    # 
    json_api_url = 'https://www.bundesregierung.de/breg-de/suche/1000408!searchJson'

    # 
    main_url = "https://www.bundesregierung.de"

    # 
    links_regpk_selector = "regierungspressekonferenz"

    # 
    csrf = ""

    def start_requests(self):
        """
        Initially make request to csrf-service to set the csrf-prevention-cookie.
        :return:
        """
        yield scrapy.Request(self.start_url_csrf, callback=self.search_with_csrf)

    def search_with_csrf(self, response):
        """
        Retrieve csrf-token from page
        :param response: scrapy.response
        :return: scrapy.request[]
        """

        # 
        jsonresponse = json.loads(response.body_as_unicode())
        self.csrf = jsonresponse["token"]

        # 
        yield scrapy.Request(self.json_api_url,
                         method="POST",
                         body='{{"search":{{"query":"","zipCodeCityQuery":"","sortOrder":"sortDate asc","page":{}}},"filters":[]}}'.format(str(164)),
                         headers={'Accept': 'application/json, text/javascript, */*; q=0.01', 'Content-Type': 'application/json', 'X-CSRF-TOKEN': self.csrf},
                         callback=self.follow_search_results)

    def follow_search_results(self, response):
        """

        :param response: 
        :return: 
        """
        # 
        jsonresponse = json.loads(response.body_as_unicode())

        if "result" in jsonresponse:
            # 
            if "items" in jsonresponse["result"]:
                for item in jsonresponse["result"]["items"]:
                    if "payload" in item:
                        # 
                        # 
                        soup = BeautifulSoup(item['payload'].replace('\n', ''), 'lxml')
                        link = self.main_url + soup.find_all('a', href=True)[0].get('href')
                        if self.links_regpk_selector in link:
                            yield scrapy.Request(link)

            # 
            if ("pageCount" in jsonresponse["result"]) and ("page" in jsonresponse["result"]):
                if jsonresponse["result"]["page"] < jsonresponse["result"]["pageCount"]:
                    yield scrapy.Request(self.json_api_url, method="POST",
                                         body='{{"search":{{"query":"","zipCodeCityQuery":"","sortOrder":"sortDate asc","page":{}}},"filters":[]}}'.format(
                                             str(jsonresponse["result"]["page"]+1)), headers={'Accept': 'application/json, text/javascript, */*; q=0.01',
                                                  'Content-Type': 'application/json', 'X-CSRF-TOKEN': self.csrf}, callback=self.follow_search_results)

    def parse(self, response):
        """
        
        :param response:
        :return:
        """

        # 
        regpk_shorttext = response.xpath('//div[@class="bpa-short-text"]//p').extract_first()
        regpk_shorttext = remove_tags(regpk_shorttext) # 

        # 
        regpk_fulltext = response.xpath('//div[@class="bpa-richtext"]/div[@class="bpa-richtext"]//p/text()').extract()

        # 
        regpk_time_extracted = response.xpath('//span[@class="bpa-time"]/time/@datetime').extract_first()
        #
        regpk_time = datetime.datetime.strptime(regpk_time_extracted, '%Y-%m-%dT%H:%M:%SZ')

        #
        yield {"href": response.url, "shorttext": regpk_shorttext, "fulltext": regpk_fulltext, "date": regpk_time}
