# Workshop 

## Ankündigung
Der Workshop führt die TeilnehmerInnen in die Techniken des Web-Crawling/Scraping ein. Es wird unter anderem auf Browser-gestützte Methoden eingegangen werden, welche zunehmend eine Rolle spielen, da häufig Browserumgebungen bzw. Nutzerinteraktionen erforderlich sind um an bestimmte Inhalte zu gelangen. Zugleich soll gemeinsam das methodische Wissen in einem praktischen Teil vertieft werden. Ziel des Workshops ist es einen Einblick in die theoretischen Hintergründe zum Crawling/Scraping anzubieten und die verschiedenen Aspekte und Teilaufgaben, welche innerhalb eines Projektes zu beachten sind, identifizieren zu können sowie dadurch den Aufwand bzw. die Durchführbarkeit grob einzuschätzen. Die Veranstaltung richtet sich sowohl an Einsteigerinnen und Einsteiger, als auch an Forscherinnen und Forscher die bereits Programmiererfahrung in diesem Themenbereich haben.
 
## Organisation
Das [Wiso-Forschungslabor](https://www.wiso.uni-hamburg.de/forschung/forschungslabor.html) der [WiSo-Fakultät der Universität Hamburg](https://www.wiso.uni-hamburg.de/)

## Ort/Zeit der Veranstaltung:
Datum: Freitag, 24.Januar 2020
Beginn: 13:00 Uhr
Ende: ca. 17:00 Uhr
Ort: Von-Melle-Park 5, Aufgang D, 1. Stockwerk, Raum 1004

Um die Veranstaltung planen zu können ist eine Anmeldung erforderlich. Richten Sie diese bitte an forschungslabor.wiso@uni-hamburg.de. Die Veranstaltung wird aufgezeichnet und über das Portal Lecture2Go zur Verfügung gestellt.

## Praktische Aufgaben
Siehe Datei ```02_presentation_anwendung.pdf``` S. 7ff. Weiteres Material zur Lösung der Aufgaben ist unter ```code-task-snippets/```.

## Lösungen
###
Werden im Laufe des Workshops freigeschaltet.
### [Aufgabe 1 und 2](https://gitlab.rrz.uni-hamburg.de/forschungslabor_digitale_methoden/regpkr)
### [Aufgabe 3](https://gitlab.rrz.uni-hamburg.de/forschungslabor_digitale_methoden/regpkpython)
### [Aufgabe 4](https://gitlab.rrz.uni-hamburg.de/forschungslabor_digitale_methoden/immo)
